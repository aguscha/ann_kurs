﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataCore.Entity;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels
{
    public class ComboHomeViewModel
    {
        public IEnumerable<Kitchen> Kitchens { get; set; }
        public IEnumerable<Dish> Dishes { get; set; }

        public IEnumerable<Recipe> Recipes { get; set; }

        public IEnumerable<Ingredient> Ingredients { get; set; } 
    }
}