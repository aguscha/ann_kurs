﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels
{
    public class DishViewModel
    {
        [Required]
        [Display(Name="Название")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Стоимость")]
        public float Cost { get; set; }

        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Рейтинг")]
        public int Rating { get; set; }

        [Required]
        [Display(Name = "Название кухни")]
        public string NameKitchen { get; set; }

        [Required]
        [Display(Name = "Изображение")]
        public string NamePicture { get; set; }
    }
}