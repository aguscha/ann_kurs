﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels
{
    public class RecipeViewModel
    {
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Время приготовления")]
        public string TimeOfcooking { get; set; }

        //[Required]
        //[Display(Name = "Описание")]
        //public string[] Steps { get; set; }

        [Required]
        [Display(Name = "Название блюда")]
        public string NameDish { get; set; }

        [Required]
        [Display(Name = "Изображение")]
        public string NamePicture { get; set; }

        //[Required]
        //[Display(Name = "Иллюстрации")]
        //public string[] RecipeInImages { get; set; }
    }
}