﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataCore.Entity;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels
{
    public class DishInfoModel
    {
        public Dish Dish { get; set; }
    }
}