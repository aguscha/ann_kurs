﻿using System.Collections.Generic;
using DataCore.Entity;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels
{
    public class KitchenInfoViewModel
    {
        public Kitchen Kitchen { get; set; }
        public IEnumerable<Dish> Dishes { get; set; } 
    }
}