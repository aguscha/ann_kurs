﻿using System.Collections.Generic;
using System.Linq;
using DataCore.Entity;
using DataCore.Service;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Infrastructure
{
    public class ServicesFacade
    {
        private readonly DishService _dishService;
        private readonly IngredientService _ingridientService;
        private readonly KitchenService _kitchenService;
        private readonly RecipeService _recipeService;

        public ServicesFacade()
        {
            string path = AppHelper.GetApplicationPath(@"App_Data\DataFiles\");

            _ingridientService = IngredientService.InitService(path + "ingredients.xml");
            _kitchenService = KitchenService.InitService(path + "kitchen.xml");
            _dishService = DishService.InitService(path + "dish.xml");
            _recipeService = RecipeService.InitService(path + "recipe.xml");

            foreach (Kitchen kitchen in GetKitchens())
            {
                foreach (Dish dish in GetDishes())
                {
                    if (dish.NameKitchen == kitchen.Name)
                    {
                        kitchen.RegisterObserver(dish);
                    }
                }
            }
            foreach (Dish dish in GetDishes())
            {
                foreach (Recipe recipe in GetRecipes())
                {
                    if (recipe.NameDish == dish.Name)
                    {
                        dish.RegisterObserver(recipe);
                    }
                }
            }
            foreach (Recipe recipe in GetRecipes())
            {
                foreach (Ingredient ingredient in GetIngredients())
                {
                    if (ingredient.NameRecipe == recipe.Name)
                    {
                        recipe.RegisterObserver(ingredient);
                    }
                }
            }
        }

        #region Get IEnumerable

        public IEnumerable<Kitchen> GetKitchens()
        {
            return _kitchenService.GetAll();
        }

        public IEnumerable<Dish> GetDishes()
        {
            return _dishService.GetAll();
        }

        public IEnumerable<Recipe> GetRecipes()
        {
            return _recipeService.GetAll();
        }

        public IEnumerable<Ingredient> GetIngredients()
        {
            return _ingridientService.GetAll();
        }

        #endregion

        #region Get item

        public Dish GetDish(int id)
        {
            //Dish dish = _dishService.GetAll().SingleOrDefault(k => k.Id == id);
            //return dish;

            return _dishService.Find(id);
        }

        public Dish GetDish(string name)
        {
            return _dishService.Find(name);
            //Dish dish = _dishService.GetAll().SingleOrDefault(k => k.Name == name);
            //return dish;
        }

        public Kitchen GetKitchen(int id)
        {
            Kitchen kitchen = _kitchenService.GetAll().SingleOrDefault(k => k.Id == id);
            return kitchen;

            //return _kitchenService.Find(id);
        }

        public Kitchen GetKitchen(string name)
        {
            return _kitchenService.GetAll().SingleOrDefault(k => k.Name == name);
        }

        public Recipe GetRecipe(int id)
        {
            Recipe recipe = _recipeService.GetAll().SingleOrDefault(k => k.Id == id);
            return recipe;
            //return _recipeService.Find(id);
        }

        public Recipe GetRecipe(string name)
        {
            Recipe recipe = _recipeService.GetAll().SingleOrDefault(k => k.Name == name);
            return recipe;
            //return _recipeService.Find(id);
        }

        public Ingredient GetIngredient(int id)
        {
            Ingredient ingredient = _ingridientService.GetAll().SingleOrDefault(k => k.Id == id);
            return ingredient;
        }

        #endregion

        public IEnumerable<Dish> GetDishesForKitchen(int id)
        {
            return _dishService.GetAll().Where(k => k.NameKitchen == _kitchenService.Find(id).Name);
        }

        public IEnumerable<Recipe> GetRecipesForDish(int id)
        {
            return _recipeService.GetAll().Where(k => k.NameDish == _dishService.Find(id).Name);
        }

        public IEnumerable<Ingredient> GetIngredientsForDish(int id)
        {
            return _ingridientService.GetAll().Where(k => k.NameRecipe == _recipeService.Find(id).Name);
        }

        #region

        public Dish FindDish(int id)
        {
            return _dishService.Find(id);
        }

        #endregion

        #region Delete

        public bool DeleteDish(int id)
        {
            return _dishService.Delete(id);
        }

        public bool DeleteKitchen(int id)
        {
            return _kitchenService.Delete(id);
        }

        public bool DeleteRecipe(int id)
        {
            return _recipeService.Delete(id);
        }

        #endregion

        #region Add item

        public Dish AddDish(Dish item)
        {
            return _dishService.Add(item);
        }

        public Kitchen AddKitchen(Kitchen item)
        {
            return _kitchenService.Add(item);
        }

        public Recipe AddRecipe(Recipe item)
        {
            return _recipeService.Add(item);
        }

        #endregion

        #region Edit item

        public Kitchen EditKitchen(int id, Kitchen kitchen)
        {
            return _kitchenService.Edit(id, kitchen);
        }

        public Dish EditDish(int id, Dish dish)
        {
            return _dishService.Edit(id, dish);
        }

        public Recipe EditRecipe(int id, Recipe recipe)
        {
            return _recipeService.Edit(id, recipe);
        }

        #endregion
    }
}