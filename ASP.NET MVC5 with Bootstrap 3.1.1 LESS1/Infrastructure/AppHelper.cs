﻿using System.Web.Hosting;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Infrastructure
{
    public static class AppHelper
    {
        /// <summary>
        /// Получение пути к каталогу в веб-приложением
        /// </summary>
        /// <returns></returns>
        public static string GetApplicationPath()
        {
            return HostingEnvironment.ApplicationPhysicalPath;
        }

        public static string GetApplicationPath(string projectPath)
        {
            return HostingEnvironment.ApplicationPhysicalPath + projectPath;
        }
    }
}