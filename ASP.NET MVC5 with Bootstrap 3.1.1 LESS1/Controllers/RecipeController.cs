﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Infrastructure;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Controllers
{
    public class RecipeController : Controller
    {
        private readonly ServicesFacade _facade;

        public RecipeController()
        {
            _facade = new ServicesFacade();
        }
        //
        // GET: /Recipe/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Recipe/Details/5
        public ActionResult Details(int id)
        {
            var model = new RecipeInfoModel();

            model.Recipe = _facade.GetRecipe(id);

            model.Ingredients = _facade.GetIngredientsForDish(model.Recipe.Id);

            return View(model);
        }

        //
        // GET: /Recipe/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Recipe/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Recipe/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Recipe/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Recipe/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Recipe/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
