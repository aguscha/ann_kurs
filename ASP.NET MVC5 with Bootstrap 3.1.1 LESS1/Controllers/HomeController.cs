﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Infrastructure;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels;
using DataCore.Entity;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ServicesFacade _facade;

        public HomeController()
        {
            _facade = new ServicesFacade();
        }

        public ActionResult Index()
        {
             var model = new ComboHomeViewModel();

            model.Kitchens = _facade.GetKitchens();
            model.Dishes = _facade.GetDishes();

            return View(model);
        }

        public ActionResult SearchDish(string name)
        {
            var model = new DishInfoViewModel();
            model.Dish = _facade.GetDish(name);
            return View(model);
        }    

        public ActionResult SortedDishes(string sort)
        {
            var model = new ComboHomeViewModel();
            model.Kitchens = _facade.GetKitchens();
            switch (sort)
            {
                case "Name": { model.Dishes = _facade.GetDishes().OrderBy(dish => dish, new DataCore.Sort.SortOfName()); } break;
                case "Cost": { model.Dishes = _facade.GetDishes().OrderBy(dish => dish, new DataCore.Sort.SortOfCost()); }break;
                case "Rating": { model.Dishes = _facade.GetDishes().OrderBy(dish => dish, new DataCore.Sort.SortOfRating()).Reverse(); } break;
                default:
                {
                    model.Dishes = _facade.GetDishes();
                } break;
            }
            return View(model);           
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}