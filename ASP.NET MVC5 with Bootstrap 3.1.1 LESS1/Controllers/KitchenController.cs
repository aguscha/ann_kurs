﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Infrastructure;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels;
using DataCore.Entity;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Controllers
{
    public class KitchenController : Controller
    {
        private readonly ServicesFacade _facade;

        public KitchenController()
        {
            _facade = new ServicesFacade();
        }

        public ActionResult Index()
        {
            return View(;
        }

        public ActionResult Details(int id)
        {
            var model = new KitchenInfoViewModel();

            model.Kitchen = _facade.GetKitchen(id);
            model.Dishes = _facade.GetDishesForKitchen(model.Kitchen.Id);
            
            return View(model);
        }

        public ActionResult SortedDishes(string sort, int id)
        {
            var model = new KitchenInfoViewModel();
            model.Kitchen = _facade.GetKitchen(id);
            switch (sort)
            {
                case "Name": { model.Dishes = _facade.GetDishesForKitchen(id).OrderBy(dish => dish, new DataCore.Sort.SortOfName()); } break;
                case "Cost": { model.Dishes = _facade.GetDishesForKitchen(id).OrderBy(dish => dish, new DataCore.Sort.SortOfCost()); } break;
                case "Rating": { model.Dishes = _facade.GetDishesForKitchen(id).OrderBy(dish => dish, new DataCore.Sort.SortOfRating()).Reverse(); } break;
                default:
                    {
                        model.Dishes = _facade.GetDishes();
                    } break;
            }
            return View(model);
        }
    }
}
