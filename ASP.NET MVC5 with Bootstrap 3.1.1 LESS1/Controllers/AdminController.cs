﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Infrastructure;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels;
using DataCore.Entity;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Controllers
{
    public class AdminController : Controller
    {
        private readonly ServicesFacade _facade;

        public AdminController()
        {
            _facade = new ServicesFacade();
        }

        #region main
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            var model = new ComboHomeViewModel();
            model.Dishes = _facade.GetDishes();
            model.Kitchens = _facade.GetKitchens();
            model.Recipes = _facade.GetRecipes();
            return View(model);
        }

        //
        // GET: /Admin/Details/5
        public ActionResult Details(int id)
        {           
            return View();
        }

        //
        // GET: /Admin/Create
        public ActionResult Create()
        {          
            return View();
        }

        //
        // POST: /Admin/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, DishViewModel model)
        {
           try
            {
                // TODO: Add update logic here
             
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/Delete/5
        public ActionResult Delete(int id)
        {          
            return View();
        }

        //
        // POST: /Admin/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        #endregion

        #region dish
        public ActionResult ListDishes()
        {
            var model = new ComboHomeViewModel();
            model.Dishes = _facade.GetDishes();
            return View(model);
        }

        public ActionResult CreateDish()
        {
            var model = new DishViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateDish(DishViewModel model)
        {
            if (ModelState.IsValid)
            {
                _facade.AddDish(new Dish(model.Name, model.Cost, model.Description, model.Rating, model.NameKitchen, model.NamePicture));
                return RedirectToAction("ListDishes");
            }
            return View(model);
        }

        public ActionResult DeleteDish(int id)
        {
            _facade.DeleteDish(id);
            return RedirectToAction("ListDishes");
        }

        public ActionResult EditDish(int id)
        {
            var model = new DishViewModel();

            var dish = _facade.GetDish(id);

            model.Name = dish.Name;
            model.Description = dish.Description;
            model.Cost = dish.Cost;
            model.Rating = dish.Rating;
            model.NameKitchen = dish.NameKitchen;
            model.NamePicture = dish.NamePicture;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditDish(int id, DishViewModel model)
        {
            if (ModelState.IsValid)
            {
                _facade.EditDish(id, new Dish(model.Name, model.Cost, model.Description, model.Rating, model.NameKitchen, model.NamePicture));                         
                // todo: для обновления объекта по данным из модели
               return RedirectToAction("ListDishes");
            }
            return View(model);            
        }
        #endregion

        #region kitchen
        public ActionResult ListKitchenes()
        {
            var model = new ComboHomeViewModel();
            model.Kitchens = _facade.GetKitchens();
            return View(model);
        }

        public ActionResult CreateKitchen()
        {
            var model = new KitchenViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateKitchen(KitchenViewModel model)
        {
            if (ModelState.IsValid)
            {
                _facade.AddKitchen(new Kitchen(model.Name, model.Description, model.NamePicture));
                return RedirectToAction("ListKitchenes");
            }            
                return View();
        }

        public ActionResult DeleteKitchen(int id)
        {
            _facade.DeleteKitchen(id);
            return RedirectToAction("ListKitchenes");
        }

        public ActionResult EditKitchen(int id)
        {
            var model = new KitchenViewModel();

            var kitchen = _facade.GetKitchen(id);

            model.Name = kitchen.Name;
            model.Description = kitchen.Description;
            model.NamePicture = kitchen.NamePicture;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditKitchen(int id, KitchenViewModel model)
        {          
                if (ModelState.IsValid)
                {
                    _facade.EditKitchen(id, new Kitchen(model.Name, model.Description, model.NamePicture));
                }
                return RedirectToAction("ListKitchenes");     
        }
#endregion

        #region recipe
        public ActionResult ListRecipes()
        {
            var model = new ComboHomeViewModel();
            model.Recipes = _facade.GetRecipes();
            return View(model);
        }

        public ActionResult CreateRecipe()
        {
            var model = new RecipeViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateRecipe(RecipeViewModel model)
        {
            if (ModelState.IsValid)
            {
                _facade.AddRecipe(new Recipe(model.Name, model.TimeOfcooking,new[] {"gmmm"}, model.NameDish,
                    model.NamePicture, new[] {"mghg"}));
                return RedirectToAction("ListRecipes");
            }
            return View(model);
        }
        public ActionResult DeleteRecipe(int id)
        {
            _facade.DeleteRecipe(id);
            return RedirectToAction("ListRecipes");
        }
        public ActionResult EditRecipe(int id)
        {
            var model = new RecipeViewModel();

            var recipe = _facade.GetRecipe(id);

            model.Name = recipe.Name;
            model.TimeOfcooking = recipe.TimeOfcooking;
            model.NameDish = recipe.NameDish;
            model.NamePicture = recipe.NamePicture;

            return View(model);
        }

        [HttpPost]
        public ActionResult EditRecipe(int id, RecipeViewModel model)
        {
            if (ModelState.IsValid)
            {               
                string[] st=_facade.GetRecipe(id).Steps;
                string[] imgs = _facade.GetRecipe(id).RecipeInImages;
                _facade.EditRecipe(id, new Recipe( model.Name, model.TimeOfcooking, st, model.NameDish,
                    model.NamePicture, imgs));

                return RedirectToAction("ListRecipes");
            }
            return View(model);
        }
        #endregion
    }
}
