﻿using System.Collections;
using System.Globalization;
using System.Linq;
using System.Web.DynamicData;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Infrastructure;
using ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Models.ViewModels;
using DataCore.Sort;
using DataCore.Sort.SortRecipes;
using SortOfName = DataCore.Sort.SortOfName;

namespace ASP.NET_MVC5_with_Bootstrap_3._1._1_LESS1.Controllers
{
    public class DishesController : Controller
    {
        private readonly ServicesFacade _facade;

        public DishesController()
        {
            _facade = new ServicesFacade();
        }

        //
        // GET: /Dishes/
        public ActionResult Index()
        {        
            return View();
        }

        //
        // GET: /Dishes/Details/5
        public ActionResult Details(int id)
        {
            var model = new DishInfoViewModel();

            model.Dish = _facade.GetDish(id);   

            model.Recipes = _facade.GetRecipesForDish(id);
            
            return View(model);
        }
        
        // GET: /Dishes/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Dishes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                if (collection != null)
                {
                    object k=collection.Get(0);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Dishes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Dishes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Dishes/Delete/5
        public ActionResult Delete(int id)
        {       
            return View();
        }

        //
        // POST: /Dishes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult SortedRecipes(string sort, int id)
        { 
        var model = new DishInfoViewModel();
            model.Dish = _facade.GetDish(id);
            switch (sort)
            {
                case "Name":
                {
                    model.Recipes = _facade.GetRecipesForDish(id)
                        .OrderBy(recipe => recipe, new DataCore.Sort.SortRecipes.SortOfName());
                }break;
                case "Time":
                {
                    model.Recipes = _facade.GetRecipesForDish(id).OrderBy(recipe => recipe, new SortOfTime());
                }break;
                default:
                {
                    model.Recipes = _facade.GetRecipesForDish(id);
                }break;
            }
            return View(model);

        }
    }
}
