﻿using System.Collections.Generic;
using System.Linq.Expressions;
using DataCore.Entity;

namespace DataCore.Entity
{
    public class Kitchen : NamedEntity, ISubject
    {
        public string Description { get; set; }
        public string NamePicture { get; set; }

        private List<IObserver> observers; 
        public Kitchen(int id, string name, string description, string namePicture) : base(id, name)
        {
            observers= new List<IObserver>();
            Description = description;
            NamePicture = namePicture;
        }

        public Kitchen(string name, string description, string namePicture)
            : base(name)
        {
            observers = new List<IObserver>();
            Description = description;
            NamePicture = namePicture;
        }

        public Kitchen()
        {
            observers = new List<IObserver>();
        }

        public void Upgrade(string newName, string newDescription, string newNamePicture)
        {
            Name = newName;
            Description = newDescription;
            NamePicture = newNamePicture;
        }

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservesr(string newName)
        {
            foreach (var observer in observers)
            {
                observer.Update(newName);
            }
        }
    }
}
