﻿namespace DataCore.Entity
{
    public class Ingredient : NamedEntity, IObserver
    {
        public float Cost { get; set; }
        public string Quantity { get; set; }
        public string NameRecipe { get; set; }

        public Ingredient(string name, float cost, string quantity, string nameRecipe)
            : base(name)
        {
            Cost = cost;
            Quantity = quantity;
            NameRecipe = nameRecipe;
        }

        public Ingredient(int id, string name, float cost, string quantity, string nameRecipe)
            : base(id, name)
        {
            Cost = cost;
            Quantity = quantity;
            NameRecipe = nameRecipe;
        }

        public Ingredient()
        {
        }

        public void Update(string newParam)
        {
            NameRecipe = newParam;
        }
    }
}
