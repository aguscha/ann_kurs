﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DataCore.Entity
{
    public interface IObserver
    {
       void Update(string newParam);
    }
}
