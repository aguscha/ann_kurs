using System.ComponentModel.DataAnnotations;

namespace DataCore.Entity
{
    public class NamedEntity
    {
        public int Id { get; set; }
   
        [Display(Name = "Название")]
        public string Name { get; set; }

        public NamedEntity(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public NamedEntity(string name)
        {
            Name = name;
            Id = 0;
        }

        public NamedEntity()
        {
            Id = 0;
        }

    }
}