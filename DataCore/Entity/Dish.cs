﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DataCore.Entity;

namespace DataCore.Entity
{
    public class Dish : NamedEntity, IObserver, ISubject
    {
        private ISubject kitchen;
        private List<IObserver> observers; 
        public float Cost { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        public string NameKitchen { get; set; }
        public string NamePicture { get; set; }

        public Dish(int id, string name, float cost, string description, int rating, string nameKitchen, string namePicture)
            : base(id, name)
        {
            observers = new List<IObserver>();
            Cost = cost;
            Description = description;
            Rating = rating;
            NameKitchen = nameKitchen;
            NamePicture = namePicture;
        }

        public Dish(string name, float cost, string description, int rating, string nameKitchen, string namePicture)
            : base(name)
        {
            observers = new List<IObserver>();
            Cost = cost;
            Description = description;
            Rating = rating;
            NameKitchen = nameKitchen;
            NamePicture = namePicture;
        }

        public Dish()
        {
            observers = new List<IObserver>();
        }
        public void Update(string newParam)
        {
            NameKitchen = newParam;
        }

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservesr(string newName)
        {
            foreach (var observer in observers)
            {
                observer.Update(newName);
            }
        }
    }
}
