﻿using System.Collections.Generic;

namespace DataCore.Entity
{
    public class Recipe : NamedEntity, IObserver, ISubject
    {
        private List<IObserver> observers;
        public string TimeOfcooking { get; set; }
        public string[] Steps { get; set; }
        public string NameDish { get; set; }
        public string NamePicture { get; set; }
        public string[] RecipeInImages { get; set; }

        public Recipe(string name, string timeOfcooking, string[] steps, string nameDish, string namePicture,
            string[] recipeInImages)
            : base(name)
        {
            observers = new List<IObserver>();
            TimeOfcooking = timeOfcooking;
            Steps = steps;
            NameDish = nameDish;
            NamePicture = namePicture;
            RecipeInImages = recipeInImages;
        }

        public Recipe(int id, string name, string timeOfcooking, string[] steps, string nameDish, string namePicture,
            string[] recipeInImages)
            : base(id, name)
        {
            observers = new List<IObserver>();
            TimeOfcooking = timeOfcooking;
            Steps = steps;
            NameDish = nameDish;
            NamePicture = namePicture;
            RecipeInImages = recipeInImages;
        }

        public Recipe()
        {
            observers = new List<IObserver>();
        }

        public void Update(string newParam)
        {
            this.NameDish = newParam;
        }

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservesr(string newName)
        {
            foreach (var observer in observers)
            {
                observer.Update(newName);
            }
        }
    }
}
