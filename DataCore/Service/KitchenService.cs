﻿using System.Collections.Generic;
using DataCore.Entity;

namespace DataCore.Service
{
    public class KitchenService : NamedEntityService<Kitchen>
    {
        private static KitchenService _service;

        public KitchenService(string filePath) : base(filePath)
        {
        }

        public static KitchenService InitService(string filePath)
        {
            if (_service != null)
            {
                return _service;
            }
            _service = new KitchenService(filePath);
            return _service;
        }

        public List<Kitchen> GetAll()
        {
            return items.GetAll();
        }

        public Kitchen EditKitchen(int id, Kitchen kitchen)
        {
            items.Find(id).NotifyObservesr(kitchen.Name);

            return items.Edit(id, kitchen);
        }
    }
}