﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using DataCore.Entity;
using DataCore.HashTable;

namespace DataCore.Service
{
    public abstract class NamedEntityService<T>
    {
        protected static bool isExist = false;
        private readonly string _filePath;
        private readonly XmlSerializer x;
        private string filePath;
        protected HashTable2<T> items;

        protected NamedEntityService(string filePath)
        {
            var items = new List<T>();
            _filePath = filePath;
            x = new XmlSerializer(items.GetType());
            TextReader reader = new StreamReader(filePath);
            items = (List<T>) x.Deserialize(reader);
            this.items = new HashTable2<T>();
            foreach (T item in items)
            {
                this.items.Add(item);
            }
            reader.Close();
        }

        public void Close()
        {
            var fileStream = new FileStream(_filePath, FileMode.OpenOrCreate);
            x.Serialize(fileStream, items);
            fileStream.Close();
        }


        public T Add(T item)
        {
            return items.Add(item);
        }

        public bool Delete(int id)
        {
            return items.Delete(id);
        }

        public List<T> GetAll()
        {
            return items.GetAll();
        }

        public T Find(int id)
        {
            return items.Find(id);
        }

        public T Find(string name)
        {
            return items.Find(name);
        }
        public T Edit(int id, T item)
        {
            (items.Find(id) as ISubject).NotifyObservesr((item as NamedEntity).Name);

            return items.Edit(id, item);
        }
    }
}