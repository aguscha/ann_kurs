﻿using System.Collections.Generic;
using DataCore.Entity;

namespace DataCore.Service
{
    public class RecipeService : NamedEntityService<Recipe>
    {
        private static RecipeService _service;

        public RecipeService(string filePath) : base(filePath)
        {
        }

        public static RecipeService InitService(string filePath)
        {
            if (_service != null)
            {
                return _service;
            }
            _service = new RecipeService(filePath);
            return _service;
        }

        public List<Recipe> GetAll()
        {
             return items.GetAll();          
        }       
    }
}
