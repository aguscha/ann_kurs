﻿using DataCore.Entity;

namespace DataCore.Service
{
    public class DishService : NamedEntityService<Dish>
    {
        private static DishService _service;

        public DishService(string filePath) : base(filePath)
        {
        }

        public static DishService InitService(string filePath)
        {
            if (_service != null)
            {
                return _service;
            }
            _service = new DishService(filePath);
            return _service;
        }

        //public Dish EditDish(int id,Dish dish)
        //{
        //    items.Find(id).NotifyObservesr(dish.Name);

        //    return items.Edit(id, dish);
        //}
    }
}