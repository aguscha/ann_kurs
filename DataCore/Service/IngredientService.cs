﻿using System.Collections.Generic;
using System.IO;
using DataCore.Entity;
using DataCore.HashTable;

namespace DataCore.Service
{
    public class IngredientService
    {
        protected HashTable<Ingredient> items;
        private System.Xml.Serialization.XmlSerializer x;
        private readonly string _filePath;
        protected static bool isExist = false;
        private string filePath;
        private static IngredientService _service;

        protected IngredientService(string filePath)
        {
            var items = new List<Ingredient>();
            _filePath = filePath;
            x = new System.Xml.Serialization.XmlSerializer(items.GetType());
            TextReader reader = new StreamReader(filePath);
            items = (List<Ingredient>) x.Deserialize(reader);
            this.items = new HashTable<Ingredient>();
            foreach (var item in items)
            {
                this.items.Add(item);
            }
            reader.Close();
        }

        public void Close()
        {
            var fileStream = new FileStream(_filePath, FileMode.OpenOrCreate);
            x.Serialize(fileStream, items);
            fileStream.Close();
        }


        public Ingredient Add(Ingredient item)
        {
            return items.Add(item);
        }

        public bool Delete(int id)
        {
            return items.Delete(id);
        }

        public List<Ingredient> GetAll()
        {
            return items.GetAll();
        }

        public Ingredient Find(int id)
        {
            return items.Find(id);
        }

        public static IngredientService InitService(string filePath)
        {
            if (_service != null)
            {
                return _service;
            }
            else
            {
                _service = new IngredientService(filePath);
                return _service;
            }
        }
    }
}