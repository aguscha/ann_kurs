﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Entity;

namespace DataCore.Sort.SortRecipes
{
   public class SortOfTime:IComparer<Recipe>
    {
       public int Compare(Recipe x, Recipe y)
       {
           return String.Compare(x.TimeOfcooking, y.TimeOfcooking);
       }
    }
}
