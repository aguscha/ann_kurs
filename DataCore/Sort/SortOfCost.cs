﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Entity;

namespace DataCore.Sort
{
    public class SortOfCost:IComparer<Dish>
    {
        public int Compare(Dish x, Dish y)
        {
            if (x.Cost < y.Cost)
                return -1;
            else
            {
                if (x.Cost > y.Cost)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
