﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Entity;

namespace DataCore.Sort
{
    public class SortOfName:IComparer<Dish>
    {
        public int Compare(Dish x, Dish y)
        {          
            return String.Compare(x.Name, y.Name);
        }
    }
}
