﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataCore.Entity;

namespace DataCore.Sort
{
    public class SortOfRating:IComparer<Dish>
    {
        public int Compare(Dish x, Dish y)
        {
            if (x.Rating < y.Rating)
                return -1;
            else
            {
                if (x.Rating > y.Rating)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
