﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DataCore.Entity;

namespace DataCore.HashTable
{
    public class HashTable2<T>
    {

        private T[] table;

        private int size;
        private int count;

        public HashTable2()
        {
            size = 51;
            table = new T[size];
        }
        public HashTable2(int size)
        {
            this.size = size;
            table = new T[size];
        }


        public int HashFunction(string name)
        {
            int h = 0, a = 127;
            foreach (char ch in name)
                h = (a * h + ch) % size;
            return h;
        }

        public int Count
        {
            get
            {
                return count;
            }
        }

        public T Insert(T item) 
        {
            int a = 0;
            int i = HashFunction((item as NamedEntity).Name);
            while (table[i] != null)
            {
                i = (i + a ^ 2) % size;
                a++;
            }
            (item as NamedEntity).Id = i;
            table[i] = item;
            count++;
            return table[i];
        }

        public bool Remove(T item)
        {
            int a = 0, b = 0;
            int i = HashFunction((item as NamedEntity).Name), j;
            while (table[i] != null)
                if ((item as NamedEntity).Name == (table[i] as NamedEntity).Name)
                    break;
                else
                {
                    i = (i + a ^ 2) % size;
                    a++;
                }

            if (table[i] == null)
                return true;
            else
            {
                table[i] = default(T);
                count--;
                for (j = i + 1; table[j] != null; j = (i + b ^ 2) % size, count--)
                {
                    T newItem = table[j];
                    table[j] = default(T);
                    Insert(newItem);
                    b++;
                }
                return true;
            }            
        }


        public T Search(T item)
        {
            int a = 0;
            int i = HashFunction((item as NamedEntity).Name);
            while (table[i] != null)
            {
                if ((item as NamedEntity).Name == (table[i] as NamedEntity).Name)
                {
                    return table[i];
                }
                else
                {
                    i = (i + a ^ 2) % size;
                    a++;
                }
            }
            return default(T);
        }
        public T Search(string name)
        {
            int a = 0;
            int i = HashFunction(name);
            while (table[i] != null)
            {
                if (name == (table[i] as NamedEntity).Name)
                {
                    return table[i];
                }
                else
                {
                    i = (i + a ^ 2) % size;
                    a++;
                }
            }
            return default(T);
        }


        public List<T> GetAll()
        {
            List<T> items = new List<T>();
            foreach (var item in table)
            {
                if (item != null)
                {
                    items.Add(item);
                }
            }
            return items;
        }

        public T Create(T item)
        {
            return Add(item);
        }

        public T Add(T item)
        {
            return Insert(item);
        }

        public bool Delete(int id)
        {
            return Remove(table[id]);
        }

        public T Find(int id)
        {
            return table[id];
        }

        public T Find(string name)
        {
            return Search(name);
        }

        public T Edit(int id, T item)
        {
            Delete(id);
           return Add(item);
        }
    }
}
