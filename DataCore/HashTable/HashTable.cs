﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using DataCore.Entity;

namespace DataCore.HashTable
{
     public class HashTable<T>
    {

         private readonly List<T> _items;

         public HashTable()
         {
             _items=new List<T>();
         }

        public List<T> ToList()
        {
            return _items;
        }
        public void Clear()
        {
            _items.Clear();
        }

        public bool Contains(T item)
        {
            return _items.Contains(item);
        }

        public int Count
        {
            get { return _items.Count; }
        }
         
        public bool Remove(T item)
        {
            return _items.Remove(item);
        }


        public T Find(int id)
        {
           // return (T)(object)(_items as List<NamedEntity>).SingleOrDefault(k => k.Id == id);
            foreach (T item in _items.ToList())
            {
                Object temp = item;
                if (((NamedEntity)temp).Id.Equals(id))
                {
                    return item;
                }
            }
            return default(T);
        }

        public T Add(T item)
        {
            object temp = item;
            NamedEntity findedNamedEntity = (NamedEntity)temp;
            findedNamedEntity.Id = _items.Count + 1;
            
            T existT = Find(findedNamedEntity.Id);
            while (existT != null)
            {
                existT = Find(findedNamedEntity.Id);
                findedNamedEntity.Id++;
            }
            temp = findedNamedEntity;
            item =(T)temp;
            _items.Add(item);
            return item;
        }
        public bool Delete(int id)
        {
            try
            {
                _items.Remove(Find(id));
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public List<T> GetAll()
        {
            return _items.ToList();
        }
    }
}
